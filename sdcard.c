#include "spi.h"
#include <stdio.h>
#include <stdlib.h>
#include <plib.h>




int InitializeSDCard()
{   
    // Set SS as output
    TRISDbits.TRISD4 = 0;
    TRISEbits.TRISE0 = 0;
    
    // Set MOSI to output
    TRISGbits.TRISG8 = 0;
    
    // Set MISO to input
    TRISGbits.TRISG7 = 1;
    
    // Set CLC to output
    TRISGbits.TRISG6 = 0;

    // Set SS high
    PORTDbits.RD4 = 0;    
    
    while ( timers_getElapsedMsek() < 10 );
    
    // Loop counter
    int l_Index = 0;
    
    // Hold MOSI high and execute at least 74 cycles to enable SPI mode.
    // 74 cycles corresponds to 9 bytes transmitted so we send 11 to be sure.
    for (l_Index = 0; l_Index < 11; l_Index++)
    {
        SPI2SendReceiveByte(0xFF);
    }
    
    
    // Wait for all transmissions to finish
    //while ( GetSPI2TXBufferSize() > 0 );
    
    
    // Set SS low
    //PORTDbits.RD4 = 1;
    
    // Send CMD0 and wait for R1 response
    while ( SPI2SendReceiveByte(0xFF) == 0xFF )
    {
        SPI2SendReceiveByte(0x40);
        SPI2SendReceiveByte(0x00);
        SPI2SendReceiveByte(0x00);
        SPI2SendReceiveByte(0x00);
        SPI2SendReceiveByte(0x00);
        SPI2SendReceiveByte(0x95);
    }
    
    
    // Wait for all transmissions to finish
    //while ( GetSPI2TXBufferSize() > 0 );


    // Set SS high
    //PORTDbits.RD4 = 1;
    
    // Get
    unsigned int l_mSekStart = timers_getElapsedMsek();
    
    // Push data until we get R1 response or indicate failure after 100 msek
    // has passed.
    //for ( l_Index = 0; l_Index < 60; l_Index++ )
    //    SPI2SendReceiveByte(0xFF);
    
    SPI2SendReceiveByte(0xFF);
    
    /*while ( 1 )
    {
        // Return error if we fail to get response after 100 msek.
        if ( (GetElapsedMsek() - l_mSekStart) > 100 )
        {
            return 0;
        }
        
        // Feed data if empty
        if ( GetSPI2TXBufferSize() < 1 )
        {
            SPI2TransmitByte(0xFF);
        }
    }*/
}
