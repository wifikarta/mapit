/* 
 * File:   main.c
 * Author: LT-GH5X5
 *
 * Created on den 27 januari 2016, 18:22
 */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <plib.h>
#include <xc.h>
#include "timers.h"
#include "uart.h"
#include "spi.h"
#include "sdcard.h"




// DEVCFG3
// USERID = No Setting

// DEVCFG2
#pragma config FPLLIDIV = DIV_2         // PLL Input Divider (2x Divider)
#pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier)
#pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)

// DEVCFG1
#pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
#pragma config FSOSCEN = ON             // Secondary Oscillator Enable (Enabled)
#pragma config IESO = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD = XT             // Primary Oscillator Configuration (XT osc mode)
#pragma config OSCIOFNC = OFF           // CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config FPBDIV = DIV_2           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/2)
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor Selection (Clock Switch Disable, FSCM Disabled)
#pragma config WDTPS = PS1024           // Watchdog Timer Postscaler (1:1024)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled (SWDTEN Bit Controls))

// DEVCFG0
#pragma config DEBUG = OFF              // Background Debugger Enable (Debugger is disabled)
#pragma config ICESEL = ICS_PGx2        // ICE/ICD Comm Channel Select (ICE EMUC2/EMUD2 pins shared with PGC2/PGD2)
#pragma config PWP = OFF                // Program Flash Write Protect (Disable)
#pragma config BWP = OFF                // Boot Flash Write Protect bit (Protection Disabled)
#pragma config CP = OFF                 // Code Protect (Protection Disabled)




/*
 * 
 */
int main(int argc, char** argv)
{
    // Enable multi vector mode
    INTCONbits.MVEC = 1;
    
    // Enable global interrupts
    __asm("ei");
    
    
    // Initialize timer2
    timers_initTimer2();
    
    // Initialize UART1
    InitUART1();
    
    // Initialize SPI
    //InitSPI2();
    
    
    // Delay for a short while before reading the SD card
    while ( timers_getElapsedMsek() < 10 );
    
    
    // Initialize SD card
    //InitializeSDCard();
    
    
    // Enable LED
    TRISFCLR = 0x01;
    PORTFbits.RF0 = 1;
    
    TRISECLR = 0x01;
    PORTEbits.RE0 = 1;
    
    //TRISDCLR = 0x10;
    //PORTDbits.RD4 = 1;
    
    // Main loop
    while (1)
    {
        if ( !(timers_getElapsedMsek() % 500) )
        {   
            //PORTFbits.RF0 = !PORTFbits.RF0;
            //PORTEbits.RE0 = !PORTEbits.RE0;
        }
    }
    
    return (EXIT_SUCCESS);
}