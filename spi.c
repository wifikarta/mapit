#include "spi.h"
#include <stdio.h>
#include <stdlib.h>
#include <plib.h>




/*
 * 
 */
char GetFirst0Bit(char p_Byte);
void InsertIntoRXStream(char p_Byte, char p_StartBit, char p_EndBit);




/*
 * DEFINES
 */
#define SPITXBUFFERSIZE 2048
#define SPIRXBUFFERSIZE 2048




/*
 *  Enums
 */
typedef enum {IDLE,ACTIVE} SPIState;




/*
 * Globals
 */
static unsigned char sg_SPI2TXBuffer[SPITXBUFFERSIZE];
static unsigned char sg_SPI2RXBuffer[SPIRXBUFFERSIZE];


// Bytes to send
static unsigned int sg_SPI2TXCount = 0;


// Stores how many bits that have been read into the RX buffer
static unsigned int sg_SPI2RXBitCount = 0;

// 
static SPIState     sg_SPIRXState = IDLE;




/*
 *
 */
void InitSPI2()
{
    // Disable I/O interrupts
    IEC1bits.SPI2RXIE = 0;
    IEC1bits.SPI2TXIE = 0;
    
    // Stop SPI
    SPI2CONbits.ON = 0;
    
    // Clear the receive buffer
    int l_Junk = SPI2BUF;
    
    // Clear SPI interrupt flags in IFS
    IFS1bits.SPI2RXIF = 0;    
    IFS1bits.SPI2TXIF = 0;
    
    // Enable I/O interrupts
    //IEC1bits.SPI2RXIE = 1;
    //IEC1bits.SPI2TXIE = 1;
    
    // Set SPI interrupt vector priority (7)
    //IPC7bits.SPI2IP = 0b111;
    
    // Set SPI interrupt vector sub priority (3))
    //IPC7bits.SPI2IS = 0b11;
    
    // Set baud rate (Page 787)
    SPI2BRG = 63;
    
    // Reset receive overflow flag
    SPI2STATbits.SPIROV = 0;
    
    // Set master mode
    SPI2CONbits.MSTEN = 1;
    
    // Set 8-bit mode
    SPI2CONbits.MODE16 = 0;
    SPI2CONbits.MODE32 = 0;
    
    // Set SMP mode to read data in-between pulses
    //SPI2CONbits.SMP = 1;
    
    // Toggle on positive clock edge
    SPI2CONbits.CKE = 0;
    
    // Set clock polarity to idle low.
    SPI2CONbits.CKP = 0;
    
    // Enable SPI
    SPI2CONbits.ON = 1;
}




/*
 *
 */
void __ISR(_SPI_2_VECTOR, IPL7SRS) ih_spi2( void )
{
    //if ( SPICONbits.SSPOV )
    {
        
    }
    
    // Handle RX interrupts
    if ( IFS1bits.SPI2RXIF )
    {
        // Read data from buffer
        //char l_BufferData = SPI2BUF;
        
        // We've managed to send 8 bits
        sg_SPI2TXCount--;
        
        
        /*switch ( sg_SPIRXState )
        {
            case IDLE:
                
                // In idle mode we wait for any data that isn't 0xFF
                if ( l_BufferData != (char)0xFF )
                {
                    // Enable active mode
                    sg_SPIRXState = ACTIVE;
                    
                    // Insert data
                    InsertIntoRXStream(l_BufferData, GetFirst0Bit(l_BufferData), 8);
                }
                
                //
                break;
                
                
            case ACTIVE:
                
                // Insert data
                InsertIntoRXStream(l_BufferData, 0, 8);
                
                //
                break;
                
            default:
                break;
        }*/
        
        
        // Read data
        sg_SPI2RXBuffer[sg_SPI2RXBitCount++] = SPI2BUF;
        
        // Check if we should keep sending
        if ( sg_SPI2TXCount > 1 )
        {
            SPI2BUF = sg_SPI2TXBuffer[sg_SPI2TXCount - 1];
        }

        
        if ( sg_SPI2RXBitCount >= 60 )
        {
            IFS1bits.SPI2RXIF = 0;
            sg_SPI2RXBitCount = 0;
        }
        
        // Clear interrupt flag
        IFS1bits.SPI2RXIF = 0;
    }
    
    
    // Handle TX interrupts
    if ( IFS1bits.SPI2TXIF )
    {
        // Clear interrupt flag
        IFS1bits.SPI2TXIF = 0;
    }
}




/*
 * 
 */
unsigned char SPI2SendReceiveByte(unsigned char p_Byte)
{
    // Wait until transmit buffer is empty
    while ( !SPI2STATbits.SPITBE );
    
    //sg_SPI2TXBuffer[sg_SPI2TXCount++] = p_Byte;
    
    // Transmit data
    SPI2BUF = p_Byte;
    
    // Wait until we have received the data back
    while ( !SPI2STATbits.SPIRBF );
    
    // Reset
    if ( sg_SPI2RXBitCount >= 2048 )
    {
        sg_SPI2RXBitCount = 0;
    }
    
    // Save data
    sg_SPI2RXBuffer[sg_SPI2RXBitCount++] = SPI2BUF;
    
    return sg_SPI2RXBuffer[sg_SPI2RXBitCount - 1];
    
    /*
    // Stall program when the buffer is full
    while ( sg_SPI2TXCount >= SPITXBUFFERSIZE );
    
    // Insert data into transmit buffer
    sg_SPI2TXBuffer[sg_SPI2TXCount++] = p_Byte;
    
    // Check if there is data in the pipeline
    if ( SPI2STATbits.SPITBE )
    {
        // Send byte
        SPI2BUF = sg_SPI2TXBuffer[sg_SPI2TXCount - 1];
    }*/
}




/*
 * 
 */
int GetSPI2TXBufferSize()
{
    return sg_SPI2TXCount;
}




/*
 * 
 */
void SPI2FlushTXBuffer()
{
    // Reset
    sg_SPI2TXCount = 0;
}




/*
 * 
 */
char GetFirst0Bit(char p_Byte)
{
    // Loop counter
    char l_Index;

    // Track index of first 0
    char l_0BitIndex = 0;

    // Find first 0 bit
    for ( l_Index = 0; l_Index < 8; l_Index++ )
    {
        // If bit is 
        if ( !((p_Byte >> (7 - l_Index)) & 0x01) )
        {
            // Return index
            return l_Index;
        }
    }

    // Return error
    return -1;
}




/*
 * 
 */
void InsertIntoRXStream(char p_Byte, char p_StartBit, char p_EndBit)
{
    return;
    // Sanity check
    if ( p_EndBit <= p_StartBit )
    {
        // TODO : LOG ERROR
        
        // 
        return;
    }
    
    // Loop index
    char l_Index;
    
    // Write bits
    for ( l_Index = 0; l_Index < p_EndBit; l_Index++ )
    {
        // Get the RX byte that will fit the data
        int l_RXByte = sg_SPI2RXBitCount % 8;
        
        // Find where in the byte we should write
        char l_BitIndex = sg_SPI2RXBitCount - (l_RXByte * 8);
        
        // Write data to RX buffer
        sg_SPI2RXBuffer[l_RXByte] = 0xDE;
    }
}