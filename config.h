/* 
 * File:   config.h
 * Author: LT-GH5X5
 *
 * Created on den 9 februari 2016, 12:02
 */

#ifndef CONFIG_H
#define	CONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    
#define _SUPPRESS_PLIB_WARNING
#define _DISABLE_OPENADC10_CONFIGPORT_WARNING


#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */

