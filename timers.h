/* 
 * File:   timers.h
 * Author: LT-GH5X5
 *
 * Created on den 5 februari 2016, 12:46
 */

#ifndef TIMERS_H
#define	TIMERS_H


/*
 * Includes
 */
#include "config.h"




/*
 * Time struct
 */
typedef struct
{
    unsigned int elapsedSek;
    unsigned int elapsedmSek;
} timers_timeState;




/*
 * 
 */
void            timers_initTimer2();
unsigned int    timers_getElapsedMsek();


#endif	/* TIMERS_H */

