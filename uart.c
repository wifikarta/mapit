#include "uart.h"
#include <stdio.h>
#include <stdlib.h>
#include <plib.h>




/*
 * Globals
 */
static char gs_UART1RXBuffer[255];
static int  gs_UART1RXCount = 0;





/*
 *
 */
void InitUART1()
{
    // PBCKL is 40MHz
    //      BAUD         BRG
    //      9600         259
    //      38400        64
    U1BRG = 259;
    
    // Set 8 bit with no parity
    U1MODEbits.PDSEL = 0b00;
    
    // Set 1 stop bit
    U1MODEbits.STSEL = 0;
 
    // Clear interrupt flag
    IFS0bits.U1RXIF = 0;
    
    // Enable UART send and transmit interrupt.
    IEC0bits.U1RXIE = 1;
    IEC0bits.U1TXIE = 0;
    
    // Set UART interrupt vector priority
    IPC6bits.U1IP = 0b111;
    
    // Set UART interrupt vector sub-vector priority
    IPC6bits.U1IS = 0b11;
    
    // Enable UART send and transmit
    U1STAbits.URXEN = 1;
    U1STAbits.UTXEN = 0;
    
    // Enable UART module
    U1MODEbits.UARTEN = 1;
}




/*
 *
 */
void __ISR(_UART_1_VECTOR, IPL7SRS) ih_uart1( void )
{
    // Handle RX interrupts
    if ( IFS0bits.U1RXIF )
    {
        // Read data to buffer
        gs_UART1RXBuffer[gs_UART1RXCount++] = U1RXREG;

        // Clear interrupt flag
        IFS0bits.U1RXIF = 0;

        // 
        if ( gs_UART1RXCount > 255 )
        {
            gs_UART1RXCount = 0;

            //TRISFbits.TRISF0 = 0;
            //PORTFbits.RF0 = !PORTFbits.RF0;
        }
    }
    
    // Handle TX interrupts
    if ( IFS0bits.U1TXIF )
    {
        // Reset interrupt flag
        IFS0bits.U1TXIF = 0;
    }
}