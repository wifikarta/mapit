/* 
 * File:   spi.h
 * Author: LT-GH5X5
 *
 * Created on den 5 februari 2016, 14:42
 */

#ifndef SPI_H
#define	SPI_H


/*
 * Includes
 */
#include "config.h"


void    InitSPI2();
int     GetSPI2TXBufferSize();
unsigned char SPI2SendReceiveByte(unsigned char p_Byte);
void    SPI2FlushTXBuffer();

#endif	/* SPI_H */

