#include "timers.h"
#include <stdio.h>
#include <stdlib.h>
#include <plib.h>




/*
 * Globals
 */
static timers_timeState g_timeState0;
static timers_timeState g_timeState1; 
static int              g_timeMutex     = 0;





/*
 *
 */
void timers_initTimer2()
{
    // Reset clock
    T2CON = 0x0;
    
    // Null counter
    TMR2 = 0x0;
    
    // Set pre-scale to 64
    T2CONbits.TCKPS = 0b110;
    
    // Set timer interrupt vector bits
    IPC2bits.T2IP = 0b111;
    
    // Set timer interrupt sub vector bits
    IPC2bits.T2IS = 0b11;
    
    // Set limit to 40 000 000 / 64 / 1000 cycles
    PR2 = 625;
    
    // Clear interrupt flag
    IFS0bits.T2IF = 0;
    
    // Zero time state
    g_timeState0.elapsedSek    = 0;
    g_timeState1.elapsedmSek   = 0;
    
    // Enable timer interrupt
    IEC0bits.T2IE = 1;
    
    // Activate timer
    T2CONbits.ON = 1;
}




/*
 * Interrupt handler for timer 2
 */
void __ISR( _TIMER_2_VECTOR, IPL7SRS) ih_timer2( void )
{
    // Inc msek
    g_timeState0.elapsedmSek++;
    
    // Update seconds
    if ( !(g_timeState0.elapsedmSek % 1000) )
    {
        g_timeState0.elapsedSek++;
    }
    
    // Toggle blinking light
    if ( !(g_timeState0.elapsedmSek % 500) )
    {
        PORTFbits.RF0 = !PORTFbits.RF0;
    }
    
    // Update public timer struct if mutex is free
    if ( !g_timeMutex )
    {
        // Update data
        g_timeState1.elapsedSek     = g_timeState0.elapsedSek;
        g_timeState1.elapsedmSek    = g_timeState0.elapsedmSek;
    }
    
    // Clear interrupt flag
    IFS0bits.T2IF = 0;
}




/*
 * 
 */
unsigned int timers_getElapsedMsek()
{
    // Wait until free
    while ( g_timeMutex );
    
    // Lock mutex
    g_timeMutex = 1;
    
    // Read data
    unsigned int l_EmSek = g_timeState1.elapsedmSek;
    
    // Unlock mutex
    g_timeMutex = 0;
    
    // Return data
    return l_EmSek;
}